using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LINQ.Services
{
    public class MenuService
    {
        public static Dictionary<string, Func<int, string>> menu = new Dictionary<string, Func<int, string>>()
        {
            {"1", GetTaskNumberByUserIdMenu},
            {"2", GetTasksListForUserMenu},
            
        };

        public static string GetTaskNumberByUserIdMenu(int id)
        {
            string result = "Project name | Number of tasks\n";
            var dictionary = MainService.GetTaskNumberByUserId(id);
            foreach (var pair in dictionary)
            {
                result += ($"{pair.Key.Name}  - {pair.Value}\n");
            }
            return result;
        }
        public static string GetTasksListForUserMenu(int id)
        {
            string result = "Task id | Task name | Task Description\n";
            var list = MainService.GetTasksListForUser(id);
            foreach (var task in list)
            {
                result += ($"{task.Id} - {task.Name} - {task.Description}\n");
            }

            return result;
        }
    }
}