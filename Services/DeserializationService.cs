using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LINQ.Models;
using LINQ.Models.DTO;
using Newtonsoft.Json;
using Task = LINQ.Models.Task;

namespace LINQ.Services
{
    public class DeserializationService
    {
        private static readonly string _apiRoute = "https://bsa21.azurewebsites.net/api/";
        private static readonly string _apiTasksRoute = "Tasks";
        private static readonly string _apiProjectsRoute = "Projects";
        private static readonly string _apiTeamsRoute = "Teams";
        private static readonly string _apiUsersRoute = "Users";
        public static List<Project> Projects = new List<Project>();
        
        
        public static async System.Threading.Tasks.Task Initialize()
        {
            string projectsJson, tasksJson, usersJson, teamsJson;
            (projectsJson, tasksJson, usersJson, teamsJson) = await HttpCallFunction();
            List<ProjectDTO> projectDtos;
            List<TaskDTO> taskDtos;
            List<TeamDTO> teamDtos;
            List<UserDTO> userDtos;
            (projectDtos, taskDtos, teamDtos, userDtos) = DeserializeObjects(projectsJson, tasksJson, usersJson, teamsJson);
            GetProjectsList(projectDtos, taskDtos, teamDtos, userDtos);
        }

        private static void GetProjectsList(List<ProjectDTO> projectDtos, List<TaskDTO> taskDtos,
            List<TeamDTO> teamDtos, List<UserDTO> userDtos)
        {
            List<Team> teams = teamDtos.Select(t => new Team()
            {
                Id = t.Id,
                Name = t.Name,
                CreatedAt = t.CreatedAt,
                Users = new List<User>(),
                Projects = new List<Project>()
            }).ToList();
            List<User> users = userDtos.Select(u => new User()
            {
                Birthday = u.Birthday,
                Email = u.Email,
                Firstname = u.Firstname,
                Id = u.Id,
                Lastname = u.Lastname,
                RegisteredAt = u.RegisteredAt,
                Tasks = new List<Task>(),
                Team = null,
                Projects = new List<Project>()
            }).Join(teams,
            user => userDtos.Find(ud => ud.Id == user.Id).TeamId,
            teams => teams.Id,
            (user, teams) =>
            {
                teams.Users.Add(user);
                user.Team = teams;
                teams.Users.Find(u => u.Id == user.Id).Team = teams;
                return user;
            }).ToList();
            List<Task> tasks = taskDtos.Select(t => new Task()
            {
                Description = t.Description,
                Id = t.Id,
                Name = t.Name,
                State = t.State,
                CreatedAt = t.CreatedAt,
                FinishedAt = t.FinishedAt,
                
            }).Join(users,
                    tasks => taskDtos.Find(td => td.Id == tasks.Id).PerformerId,
                    users => users.Id,
                    (tasks, users) =>
                    {
                        users.Tasks.Add(tasks);
                        tasks.Performer = users;
                        return tasks;
                    }).ToList();
            List<Project> projects = projectDtos.Select(p => new Project()
            {
                Deadline = p.Deadline,
                Description = p.Description,
                Id = p.Id,
                Name = p.Name,
                CreatedAt = p.CreatedAt
            }).GroupJoin(tasks,
                project => project.Id,
                tasks => taskDtos.Find(td => td.Id == tasks.Id).ProjectId,
                (project, tasks) =>
                {
                    tasks.ToList().ForEach(t => t.Project = project);
                    project.Tasks.AddRange(tasks);
                    return project;
                }
            ).Join(teams,
                    project => projectDtos.Find(pd => pd.Id == project.Id).TeamId,
                    teams => teams.Id,
                    (project, teams) =>
                    {
                        project.Team = teams;
                        teams.Projects.Add(project);
                        return project;
                    })
                .Join(users,
                    project => projectDtos.Find(pd => pd.Id == project.Id).AuthorId,
                    users => users.Id,
                    (project, users) =>
                    {
                        project.Author = users;
                        users.Projects.Add(project);
                        return project;
                    }).ToList();
            Projects = projects;

            /*projectDtos.GroupJoin(taskDtos,
                projectDto => projectDto.Id,
                taskDtos => taskDtos.ProjectId,
                (projectDto, taskDtos ) => new Project()
                {
                    Description = projectDto.Description,
                    Deadline = projectDto.Deadline,
                    Id = projectDto.Id,
                    Name = projectDto.Name,
                    
                }
                )*/
            /*
            projectDtos.Join(teamDtos,
                    project => project.TeamId,
                    teamDtos => teamDtos.Id,
                    (project, teamDtos) => new
                        {
                            project = project, team = teamDtos,
                        }).Join (userDtos,
                            project => project.team.Id,
                            userDtos => userDtos.TeamId,
                            (project, userDtos) => new
                            {
                                project = project.project, team = project.team, teammates = userDtos 
                            }
                         ).Join(taskDtos,
                            project => project.project.Id,
                            taskDtos => taskDtos.ProjectId,
                            (project, taskDtos) => new
                            {
                                project = project.project, team = project.team, teammates = project.teammates, task = taskDtos
                            }
                        ).Join(userDtos,
                            project => project.project.AuthorId,
                            userDtos => userDtos.Id,
                            (project, userDtos) => new
                            {
                                project = project.project, projectAuthor = userDtos, team = project.team, teammates = project.teammates, task = project.task
                            }
                        ).Join(userDtos,
                            project => project.task.PerformerId,
                            userDtos => userDtos.Id,
                            (project, userDtos) => new
                            {
                                project = project.project, projectAuthor = project.projectAuthor,
                                team = project.team, teammates = project.teammates, 
                                task = project.task, performers = userDtos
                            }
                        ).Select(p => new Project()
                {
                    Deadline = p.project.Deadline,
                    Description = p.project.Description,
                    Id = p.project.Id,
                    Name = p.project.Name,
                    CreatedAt = p.project.CreatedAt,
                    Team = new Team()
                    {
                        Id = p.team.Id,
                        Name = p.team.Name,
                        CreatedAt = p.team.CreatedAt,
                        Users = new List<User>()
                        {
                            
                        },
                        Projects =  new List<Project>()
                    },
                    Tasks = new List<Task>()
                    {
                        new Task()
                        {
                            Description = p.task.Description,
                            
                        }
                    }
                    Author = p.project.
                })
                .Join(
                taskDtos,
                projects => projects.Id,
                taskDtos => taskDtos.ProjectId,
                (projects, taskDtos) =>
                {
                    new Project()
                    {
                        Tasks = new List<Task>()
                        {
                            new Task()
                            {
                                Description = taskDtos.Description,
                                Id = taskDtos.Id,
                                Name = taskDtos.Name,
                                Project = projects,
                                State = taskDtos.State,
                                CreatedAt = taskDtos.CreatedAt,
                                FinishedAt = taskDtos.FinishedAt,
                                Performer = userDtos.Where(u => u.Id == taskDtos.PerformerId)
                                    .Select(u => new User()
                                    {
                                        Birthday = u.Birthday,
                                        Email = u.Email,
                                        Firstname = u.Firstname,
                                        Id = u.Id,
                                        Lastname = u.Lastname,
                                        
                                    }).First();
                                    new User()
                                {
                                    
                                }
                            }
                        }.Join(
                            userDtos,
                            taskDtos => taskDtos.,
                            userDtos => userDtos.
                            )
                    }
                    projects.Tasks.Add( new Task()
                    {
                        Description = taskDtos.Name,
                        Id = taskDtos.Id,
                        Name = taskDtos.Name,
                        Project = projects,
                        State = taskDtos.State,
                        CreatedAt = taskDtos.CreatedAt,
                        FinishedAt = taskDtos.FinishedAt,
                    });
                }
                )
                */

            /*List<Team> teams = new List<Team>();
            List<User> users = new List<User>();
            List<Task> tasks = new List<Task>();
            teamDtos.ForEach(t => teams.Add(new Team()
            {
                Id =  t.Id,
                Name = t.Name,
                CreatedAt = t.CreatedAt
            }));
            userDtos.ForEach(u => users.Add(new User()
            {
                Birthday = u.Birthday,
                Email = u.Email,
                Firstname = u.Firstname,
                Id = u.Id,
                Lastname = u.Lastname,
                RegisteredAt = u.RegisteredAt,
                Team = teams.Find(t=> t.Id == u.TeamId)
            }));
            projectDtos.ForEach( p => Projects.Add(new Project
            {
                Deadline = p.Deadline,
                Description = p.Description,
                Name = p.Name,
                CreatedAt = p.CreatedAt,
                Id = p.Id,
                Author = users.Find(u => u.Id == p.AuthorId),
                Tasks = new List<Task>(),
                Team = teams.Find( t => t.Id == p.TeamId)
            }));
            taskDtos.ForEach(t =>
            {
                tasks.Add(new Task()
                {
                    Description = t.Description,
                    Id = t.Id,
                    Name = t.Name,
                    State = t.State,
                    CreatedAt = t.CreatedAt,
                    FinishedAt = t.FinishedAt,
                    Performer = users.Find(u => u.Id == t.PerformerId)
                });
                Projects.ForEach(p => {
                    if(t.ProjectId == p.Id)
                    {
                        p.Tasks.Add(tasks.Find(tl=> tl.Id == t.Id));
                    }
                });
            });*/

        }
        
        private static  (List<ProjectDTO>, List<TaskDTO>, List<TeamDTO>, List<UserDTO>) 
            DeserializeObjects(string projects, string tasks, string users, string teams)
        {
            List<ProjectDTO> projectDtos = JsonConvert.DeserializeObject<List<ProjectDTO>>(projects);
            List<TaskDTO> taskDtos = JsonConvert.DeserializeObject<List<TaskDTO>>(tasks);
            List<TeamDTO> teamDtos = JsonConvert.DeserializeObject<List<TeamDTO>>(teams);
            List<UserDTO> userDtos = JsonConvert.DeserializeObject<List<UserDTO>>(users);
            return (projectDtos, taskDtos, teamDtos, userDtos);
        }
        private static async Task<(string, string, string, string)> HttpCallFunction()
        {
            string projectsJson, tasksJson, usersJson, teamsJson;
            projectsJson = await GetJsonByURL(_apiRoute + _apiProjectsRoute);
            tasksJson = await GetJsonByURL(_apiRoute + _apiTasksRoute);
            usersJson = await GetJsonByURL(_apiRoute + _apiUsersRoute);
            teamsJson = await GetJsonByURL(_apiRoute + _apiTeamsRoute);
            return (projectsJson, tasksJson, usersJson, teamsJson);
        }

        private static async Task<string> GetJsonByURL(string url)
        {
            string json;
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage tasksResponseMessage = await client.GetAsync(url);
                tasksResponseMessage.EnsureSuccessStatusCode();
                json = await tasksResponseMessage.Content.ReadAsStringAsync();
            }
            return json;
        }
    }
}