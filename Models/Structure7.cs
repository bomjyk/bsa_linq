namespace LINQ.Models
{
    public class Structure7
    {
        public Project Project { get; set; }
        public Task LongestProjectTaskByDescription { get; set; }
        public Task ShortestProjectTaskByName { get; set; }
        public int NumberOfPerformancers { get; set; }
    }
}