using System;
using System.Collections.Generic;
using LINQ.Models.DTO;

namespace LINQ.Models
{
    public class User
    {
        public int Id { get; set; }
        public Team? Team { get; set; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime Birthday { get; set; }
        
        public List<Task> Tasks { get; set; }
        public List<Project> Projects { get; set; }
        
        public User() {}

        public User(User copy)
        {
            Id = copy.Id;
            Team = copy.Team;
            Firstname = copy.Firstname;
            Lastname = copy.Lastname;
            Email = copy.Email;
            RegisteredAt = copy.RegisteredAt;
            Birthday = copy.Birthday;
            Tasks = copy.Tasks;
            Projects = copy.Projects;
        }

    }
}